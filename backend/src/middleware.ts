import { Request, Response, NextFunction } from "express"
import jwt from "jsonwebtoken"

interface CustomRequest extends Request {
    user: string | jwt.JwtPayload
}

export const validateToken = (req_: Request, res: Response, next: NextFunction) => {
    const req = req_ as CustomRequest
    const auth = req.get("Authorization")
    if (!auth?.startsWith("Bearer ")) return res.status(401).send("Invalid token")
    
    const token = auth.substring(7)
    const secret = process.env.SECRET ?? ""
    try {
        const decodedToken = jwt.verify(token, secret)
        req.user = decodedToken
        next()
    } catch (error) {
        return res.status(401).send("Invalid token")
    }
}

export const handleUnknownEndpoint = (req: Request, res: Response) => {
    res.status(404).send("Resource does not exist")
}

export const validateNote = (req: Request, res: Response, next: NextFunction ) => {
    const { userid, title, content } = req.body
    //check if we can change the userid string to number
    const useridNum = Number(userid)
    if (isNaN(useridNum)) {
        console.log("Failed to convert userid to number")
        return res.status(400).send("Missing or invalid parameters")
    }

    if (typeof(useridNum) !== "number" || userid === "" || typeof(title) !== "string" || title === "" || typeof(content) !== "string" || content === "") {
        return res.status(400).send("Missing or invalid parameters")
    }

    next()
}

export const validateDeletion = (req: Request, res: Response, next: NextFunction ) => {
    const { id } = req.params
    //check if we can change the userid string to number
    const useridNum = Number(id)
    if (isNaN(useridNum)) {
        console.log("Failed to convert userid to number")
        return res.status(400).send("Missing or invalid parameters")
    }

    if (typeof(useridNum) !== "number" || id === "") {
        return res.status(400).send("Missing or invalid parameters")
    }

    next()
}