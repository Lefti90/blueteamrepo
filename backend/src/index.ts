import server from "./server"

const isValidEnv = (): boolean => {
    const { PORT, PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE, SECRET, USE_SSL, VERSION } = process.env
    
    if (typeof PORT !== "string" || PORT === "") return false
    if (typeof PG_HOST !== "string" || PG_HOST === "") return false
    if (typeof PG_PORT !== "string" || PG_PORT === "") return false
    if (typeof PG_USERNAME !== "string" || PG_USERNAME === "") return false
    if (typeof PG_PASSWORD !== "string" || PG_PASSWORD === "") return false
    if (typeof PG_DATABASE !== "string" || PG_DATABASE === "") return false
    if (typeof SECRET !== "string" || SECRET === "") return false
    if (typeof USE_SSL !== "string" || USE_SSL === "") return false
    if (typeof VERSION !== "string" || VERSION === "") return false

    return true
}

if (!isValidEnv()) {
    console.log("Error: no required environment variables found")
    process.exit(1)
}

const { PORT } = process.env
server.listen(PORT, () => console.log("Server listening to port", PORT))