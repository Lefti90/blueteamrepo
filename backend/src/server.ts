import express, {Request, Response} from "express"
import { handleUnknownEndpoint } from "./middleware"
import notesRouter from "./notesRouter"
import usersRouter from "./usersRouter"

const server = express()

server.use("/", express.static("./dist/client"))

server.use(express.json())

server.use("/users", usersRouter)
server.use("/notes", notesRouter)

server.get("/version", (_req: Request, res: Response) => {
    const { VERSION } = process.env
    res.send(VERSION)
})

server.use("/", handleUnknownEndpoint)

server.get("*", (_req, res) => {
    res.sendFile("index.html", { root: "./dist/client" })
})

export default server