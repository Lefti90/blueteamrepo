import express, { Request, Response } from "express"
import usersDao from "./db/usersDao"
import argon2 from "argon2"
import jwt from "jsonwebtoken"

const router = express.Router()

router.get("/", async (_req: Request, res: Response) => {
    const response = await usersDao.getUsers()
    res.send(response)
})

router.post("/register", async (req: Request, res: Response) => {
    const { name, password } = req.body
    const hash = await argon2.hash(password)
    await usersDao.addUser(hash, name)
    res.sendStatus(201)
})

router.post("/login", async (req: Request, res: Response) => {
    const { name, password } = req.body
    
    const user = await usersDao.findUser(name)
    if (typeof user === "undefined") return res.status(400).send("Invalid login credentials")
    const userid = user.id

    const isPasswordValid = await argon2.verify(user.hash, password)
    if (!isPasswordValid) return res.status(400).send("Invalid login credentials")

    const payload = { userid: userid, name: name }
    const secret = process.env.SECRET ?? ""
    const options = { expiresIn: "31min" }
    const token = jwt.sign(payload, secret, options)
    res.send(token)
})

export default router