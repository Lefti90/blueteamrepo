import express, { Request, Response } from "express"
import notesDao from "./db/notesDao"
import { validateDeletion, validateNote, /*validateToken*/ } from "./middleware"

const router = express.Router()

router.get("/", async (_req: Request, res: Response) => {
    const result = await notesDao.getNotes()
    res.send(result)
})

router.get("/:id", async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    console.log("Backend requested for note id", id)
    const result = await notesDao.getNote(id)
    res.send(result)
})

router.post("/new", validateNote, /*validateToken,*/ async (req: Request, res: Response) => {
    const {userid, title, content} = req.body
    await notesDao.addNote(userid, title, content)
    res.sendStatus(201)
})

router.delete("/:id", validateDeletion, /*validateToken,*/ async (req: Request, res: Response) => {
    const userid = req.body.userid
    const id = Number(req.params.id)
    const result = await notesDao.deleteNote(userid, id)
    res.send(`${result}`)
})

export default router