import { queries } from "./queries"
import executeQuery from "./db"

const getUsers = async () => {
    const result = await executeQuery(queries.users.getAll)
    return result.rows
}

const getUser = async (id: number) => {
    const queryParams = [id]
    const result = await executeQuery(queries.users.find, queryParams)
    return result.rows[0]
}

const addUser = async (hash: string, name: string) => {
    const queryParams = [hash, name]
    await executeQuery(queries.users.add, queryParams)
    return
}

const findUser = async (name:string) => {
    const queryParams = [name]
    const result = await executeQuery(queries.users.find, queryParams)
    return result.rows[0]
}

export default { getUsers, getUser, addUser, findUser }