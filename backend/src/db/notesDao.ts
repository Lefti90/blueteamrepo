import { queries } from "./queries"
import executeQuery from "./db"

const getNotes = async () => {
    const result = await executeQuery(queries.notes.getAll)
    return result.rows
}

const getNote = async (id: number) => {
    const queryParams = [id]
    const result = await executeQuery(queries.notes.get, queryParams)
    return result.rows[0]
}

const addNote = async (userid: number, title: string, content: string) => {
    const queryParams = [userid, title, content]
    const result = await executeQuery(queries.notes.add, queryParams)
    return result
}

const deleteNote = async (userid: string, id: number) => {
    const queryParams = [userid, id]
    const result = await executeQuery(queries.notes.checkDeletable, queryParams)
    if(result.rows.length !== 0){
        await executeQuery(queries.notes.delete, queryParams)
        console.log(result.rows)
        return `Deleted note ${id}`
    }else{
        return "Nothin to delete.\n No note with that id \n or you don't have the rights to delete that note."
    }
}

export default { getNotes, getNote, addNote, deleteNote }