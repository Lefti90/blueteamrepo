export const queries = {
    notes: {
        getAll: "SELECT id, userid, title FROM notes;",
        get: "SELECT * FROM notes WHERE id = $1",
        add: "INSERT INTO notes (userid, title, content) VALUES ($1, $2, $3)",
        delete: "DELETE FROM notes WHERE userid=$1 AND id=$2;",
        checkDeletable: "SELECT * FROM notes WHERE userid=$1 AND id=$2;"
    },
    users: {
        getAll: "SELECT id, name FROM users;",
        find: "SELECT * FROM users WHERE name = $1;",
        add: "INSERT INTO users (hash, name) VALUES ($1, $2);"
    }
} 