import { useState } from "react"
import axios from "axios"

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const NewNoteElement = (props: any) => {
    const [userid, setId] = useState("")
    const [title, setTitle] = useState("")
    const [content, setContent] = useState("")
  
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function handleID(event: any){
        setId(event.target.value)
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function handleTitle(event: any){
        setTitle(event.target.value)
    }
  
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function handleContent(event: any){
        setContent(event.target.value)
    }

    const handleCancel = () => {
        props.setShowNewNoteElement(false)
        props.setShowNewNoteButton(true)
    }
  
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const handleSubmit = async (event: any) =>{
        event.preventDefault()
        axios.post("/notes/new", {
            userid: userid,
            title: title,
            content: content
        }).then(function (response){
            window.alert("Note added succesfully!")
            console.log(response)
            location.href = "/"
        }).catch(function (error){
            window.alert("Invalid note information")
            console.log(error)
        })
    }
    return (
        <>
            <div className="form-wrapper">
                <hr></hr>
                <h3 className="form-header">New Note</h3>
                <form className="form">
                    <label className="label">
                        User ID: 
                        <input type="text" name="userid" onChange={handleID} />
                    </label>
                    <br></br>
                    <label className="label">
                        Title: 
                        <input type="text" name="title" onChange={handleTitle} />
                    </label>
                    <br></br>
                    <label className="label">
                        Content: 
                        <textarea rows={8} name="content" onChange={handleContent}/>
                    </label>
                    <br></br>
                    <input className="submit" type="submit" value="Submit" onClick={handleSubmit}/>
                    <button className="cancel" type="reset" onClick={handleCancel}>Cancel</button>
                </form>
            </div>
        </>
    )
}