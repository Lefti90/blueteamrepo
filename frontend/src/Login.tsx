import { useState } from "react"
import axios from "axios"
import Register from "./Register"

interface User {
    id: number
    name: string
}
interface LoginProps {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    setLoggedIn: any
}

const parseJwt = async (token: string) => {
    const base64Url = token.split(".")[1]
    const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/")
    const jsonPayload = decodeURIComponent(window.atob(base64).split("").map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(""))

    return JSON.parse(jsonPayload)
}

const defaultUser: User = { id: 0, name: "default" }

const Login = (props: LoginProps) => {
    const [user, setUser] = useState(defaultUser)
    const [loginUsername, setLoginUsername] = useState("")
    const [loginPassword, setLoginPassword] = useState("")
    const [showRegistration, setShowRegistration] = useState(false)


    const loginUser = async () => {
        await axios.post("/users/login", {
            "name": `${loginUsername}`,
            "password": `${loginPassword}`
        }).then(async (response) => {

            localStorage.setItem("notebookUserToken", `${response.data}`)

            const decodedToken = await parseJwt(response.data)

            setUser({ id: decodedToken.userid, name: decodedToken.name })
            props.setLoggedIn(true)

            setLoginUsername("")
            setLoginPassword("")
        }).catch((error) => {
            window.alert("Login failed")
            console.log("Login failed:\n", error)
        })
    }

    const logoutUser = () => {
        console.log("Logout user")
        localStorage.setItem("notebookUserToken", "")
        props.setLoggedIn(false)
        setUser(defaultUser)
    }

    const toggleRegistration = () => {
        setShowRegistration(!showRegistration)
    }

    if (user.id === 0) {
        if (showRegistration) {
            return (
                <div className="register_form">
                    <Register toggleRegistration={toggleRegistration} />
                </div>
            )
        } else {
            return (
                <div className="login_wrapper">
                    <input className="login_input" placeholder="Username" type="text" value={loginUsername} onChange={e => setLoginUsername(e.target.value)}></input>
                    <input className="login_input" placeholder="Password" type="text" value={loginPassword} onChange={e => setLoginPassword(e.target.value)}></input>
                    <button className="login_button" onClick={loginUser}>Login</button>
                    <button className="login_button" onClick={toggleRegistration}>+</button>
                </div>
            )
        }
    } else {
        return (
            <div className="logout_wrapper">
                Logged in as <b>{user.name}  </b>
                <button className="logout" onClick={logoutUser}>Logout</button>
            </div>
        )
    }
}

export default Login