import axios from "axios"
import { useState, useEffect } from "react"
import { parseJwt } from "./ParseJwt"



// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const DeleteNoteElement = (props: any) => {
    const [deleteid, setDeleteid] = useState<string>()
    const [userid, setUserid] = useState<string>()
    let userToken: string

    const handleCancel = () => {
        props.setShowDeleteNoteElement(false)
        props.setShowDeleteNoteButton(true)
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    function handleDeletid(event: any){
        setDeleteid(event.target.value)
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const handleDeletion = async (event: any) => {
        event.preventDefault()

        if(String(deleteid) !== "" || deleteid !== undefined ){
            //check the user id
            userToken = (String(localStorage.getItem("notebookUserToken")))
        }
        const uncodedToken = await parseJwt(userToken)
        setUserid(uncodedToken.userid)
    }

    const deleteNote = async () =>{
        await axios.delete(`/notes/${deleteid}`, {
            data: {
                userid: userid,
                id: deleteid
            }
        }).then(function (response){
            console.log(response)
            window.alert(`${response.data}`)
            // location.href = "/"
            setUserid("")
        }).catch(function (error){
            window.alert("Invalid note information")
            console.log(error)
            setUserid("")
        })
    }

    // Run deleteNote when userid is set
    // Double check because initial state is not empty string
    useEffect(() => {
        if (userid !== "" || userid){
            if (userid && deleteid) {
                deleteNote()
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [userid, deleteid])

    if(!props.isLoggedIn){
        return null
    }
    return(
        <div className="deleteNoteElement">
            <h3 className="delete_header">Delete note:</h3>
            <input  placeholder="Deletable note id, ie: 22" type="text" name="userid" onChange={handleDeletid} />
            <button className="delete_button" onClick={handleDeletion}>DELETE</button>
            <button className="cancel" type="reset" onClick={handleCancel}>Cancel</button>
        </div>
    )    
}