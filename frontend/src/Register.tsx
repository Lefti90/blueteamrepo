import { useState } from "react"
import axios from "axios"

interface RegisterProps {
    toggleRegistration: () => void
}

const Register = (props: RegisterProps) => {
    const { toggleRegistration } = props
    const [registerUsername, setRegisterUsername] = useState("")
    const [registerPassword, setRegisterPassword] = useState("")
    const [registerFirstName, setRegisterFirstName] = useState("")
    const [registerLastName, setRegisterLastName] = useState("")
    const [registerEmail, setRegisterEmail] = useState("")

    const isValidForm = () => {
        if (registerUsername === "") return false
        if (registerPassword === "") return false
        if (registerFirstName === "") return false
        if (registerLastName === "") return false
        if (registerEmail === "") return false

        return true
    }

    const onRegister = async () => {
        if (!isValidForm()) {
            alert("Please fill all fields")
        } else {
            await axios.post("/users/register", {
                "name": `${registerUsername}`,
                "password": `${registerPassword}`
            }).then(async (response) => {
                if (response.status === 201) {
                    alert(`User ${registerUsername} created successfully`)
                    clearRegistration()
                } else {
                    window.alert("Account registration failed")
                }
            }).catch((error) => {
                window.alert("Account registration failed")
                console.log("Login failed:\n", error)
            })
        }
    }

    const clearRegistration = () => {
        setRegisterUsername("")
        setRegisterPassword("")
        setRegisterFirstName("")
        setRegisterLastName("")
        setRegisterEmail("")
        toggleRegistration()
    }

    return (
        <div className="form-container">
            <h1>Register</h1>
            <div>
                {/* <label>Username:</label> */}
                <input type="text" placeholder="Enter username" value={registerUsername} onChange={e => setRegisterUsername(e.target.value)}></input>
            </div>
            <div>
                {/* <label>Password:</label> */}
                <input type="text" placeholder="Enter password" value={registerPassword} onChange={e => setRegisterPassword(e.target.value)}></input>
            </div>
            <div>
                {/* <label>First name:</label> */}
                <input type="text" placeholder="Enter first name" value={registerFirstName} onChange={e => setRegisterFirstName(e.target.value)}></input>
            </div>
            <div>
                {/* <label>Last name:</label> */}
                <input type="text" placeholder="Enter last name" value={registerLastName} onChange={e => setRegisterLastName(e.target.value)}></input>
            </div>
            <div>
                {/* <label>Email:</label> */}
                <input type="text" placeholder="Enter email" value={registerEmail} onChange={e => setRegisterEmail(e.target.value)}></input>
            </div>
            <div>
                <button className="btn cancel" onClick={onRegister}>Register</button>
                <button className="btn cancel" onClick={clearRegistration}>Cancel</button>
            </div>

        </div>
    )
}

export default Register