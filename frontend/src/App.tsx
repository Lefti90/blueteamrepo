import { useEffect, useState } from "react"
import { Link, Outlet } from "react-router-dom"
import "./App.css"
import { NewNoteElement } from "./NewNoteElement"
import Login from "./Login"
import { DeleteNoteElement } from "./DeleteNoteElement"

export interface Notes {
    id: number,
    userid: number,
    title: string,
    content: string
}

const App = () => {
    const [notes, setNotes] = useState<Array<Notes>>([])
    const [showNewNoteElement, setShowNewNoteElement] = useState(false)
    const [showNewNoteButton, setShowNewNoteButton] = useState(true)
    const [showDeleteNoteElement, setShowDeleteNoteElement] = useState(false)
    const [showDeleteNoteButton, setShowDeleteNoteButton] = useState(true)
    const [isLoggedIn, setLoggedIn] = useState(false)
    console.log(notes)
    
    useEffect(() => {
        initialize()
    }, [])

    const initialize = async () => {
        const result = await fetch("/notes")
        const notes = await result.json()

        setNotes(notes)
    }

    const handleAddNewNote = () => {
        setShowNewNoteElement(!showNewNoteElement)
        setShowDeleteNoteElement(false)
    }

    const handleDeleteNote = () => {
        setShowDeleteNoteElement(!showDeleteNoteElement)
        setShowNewNoteElement(false)
    }

    return (
        <div className="App">
            <Login setLoggedIn={setLoggedIn}/>
            <a href="/"><h1 className="header">Notebook-App</h1></a>
            <nav className="nav">
                {notes.map(({ id, title }) => {
                    return <li className="list" key={id}><Link className="link" key={id} to={`/notes/${id.toString()}`}><h3 className="title">{title}</h3></Link></li>
                })}
            </nav>
            <Outlet />
            {showNewNoteButton && <button className="new_note_button" onClick={() => handleAddNewNote()}>New note</button>}
            {showDeleteNoteButton && <button className="delete_note_button" onClick={() => handleDeleteNote()}>Delete note</button>}
            {showNewNoteElement && <NewNoteElement setShowNewNoteElement={setShowNewNoteElement} setShowNewNoteButton={setShowNewNoteButton}/>}
            {showDeleteNoteElement && <DeleteNoteElement isLoggedIn={isLoggedIn} setShowDeleteNoteElement={setShowDeleteNoteElement} setShowDeleteNoteButton={setShowDeleteNoteButton}/>}
        </div>
    )
}

export default App
