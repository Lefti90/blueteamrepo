import { useLoaderData } from "react-router-dom"
import { Notes } from "./App"
import "./App.css"

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function loader({ params }: any) {
    const id = Number(params.id)
    const result = await fetch(`/notes/${id}`)
    const notes = await result.json()

    return notes
}

export default function Note() {
    const notes: Notes = useLoaderData() as Notes

    return (
        <div className="Note">
            <h1 className="note_header">{notes?.title}</h1>
            <p className="note_content">{notes?.content}</p>
        </div>
    )
}
